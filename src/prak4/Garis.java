package prak4;

public class Garis {
    int[] titikA = new int[2];
    int[] titikB = new int[2];

    private void exportNilai(Titik2 a, Titik2 b){
        titikA[0] = a.getTitikX();
        titikA[1] = a.getTitikY();
        titikB[0] = b.getTitikX();
        titikB[1] = b.getTitikY();
    }

    public Garis(int A, int B, int C, int D) {
        Titik2 a = new Titik2(A, B);
        Titik2 b = new Titik2(C, D);
        exportNilai(a,b);
    }

    public Garis(double A, int B, int C, int D) {
        Titik2 a = new Titik2(A, B);
        Titik2 b = new Titik2(C, D);
        exportNilai(a,b);
    }

    public Garis(int A, double B, int C, int D) {
        Titik2 a = new Titik2(A, B);
        Titik2 b = new Titik2(C, D);
        exportNilai(a,b);
    }

    public Garis(String A, int B, int C, int D) {
        Titik2 a = new Titik2(A, B);
        Titik2 b = new Titik2(C, D);
        exportNilai(a,b);
    }

    public Garis(int A, int B, int C, String D) {
        Titik2 a = new Titik2(A, B);
        Titik2 b = new Titik2(C, D);
        exportNilai(a,b);
    }

    public Garis() {
        System.out.println("Titik awal dan akhir tidak ditentukan");
    }

    public int[] getAwal() {
        return titikA;
    }

    public int[] getAkhir() {
        return titikB;
    }
}
