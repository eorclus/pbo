package prak4;

public class TesTitik2 {
    public static void main(String[] args) {
        Titik2 a = new Titik2();
        Titik2 c = new Titik2(10.5, 10);
        Titik2 d = new Titik2(10, 10.5);
        Titik2 e = new Titik2("15", 10);
        Titik2 f = new Titik2(15, "15");
        Titik2 g = new Titik2(10, 10, 5);
        Titik2 b = new Titik2(10, 10);
        System.out.println("Titik x = "+ b.getTitikX());
        System.out.println("Titik y = "+ b.getTitikY());
    }
}
