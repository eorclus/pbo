package prak4;

class iniKotak {
    int p, l;

    public iniKotak() {
        this.p = 0;
        this.l = 0;
    }

    public iniKotak(int a, int b) {
        this.p = a;
        this.l = b;
    }

    public int getLuas(){
        return p*l;
    }
}

public class Kotak {
    public static void main(String[] args) {
        iniKotak a = new iniKotak();
        iniKotak b = new iniKotak(4,5);
        System.out.println("Luas persegi = "+b.getLuas());
    }
}
