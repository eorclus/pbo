package prak5;

public class Tugas{
    public static void main(String[] args){
        Pembeli yangbeli = new Pembeli();
        yangbeli.setPembeli("Budi");

        Product kkentang = new Product();
        kkentang.setProduct("Keripik Kentang", 3000);
        Product susu = new Product();
        susu.setProduct("Susu", 7000);
        Product gula = new Product();
        gula.setProduct("Gulaku", 7000);
        kkentang.cetakProduct();
        susu.cetakProduct();
        gula.cetakProduct();

        Product[] yangdibelibudi = new Product[3];
        yangdibelibudi[0]=kkentang;
        yangdibelibudi[1]=susu;
        yangdibelibudi[2]=gula;
        TransaksiJual beli = new TransaksiJual();
        beli.setTransaksi(yangbeli,yangdibelibudi);
        beli.cetakTransaksi();
    }
}
