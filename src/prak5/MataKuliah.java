package prak5;

public class MataKuliah {
    private String matkul;
    Dosen dosen = new Dosen();
    public MataKuliah(){
        matkul = "-";
    }
    public void setMatkul(String inmatkul, Dosen dosenIn){
        matkul = inmatkul;
        dosen = dosenIn;
    }
    public String getDosenPengajar(){
        return (dosen.getNamaDosen());
    }
    public String getMatkul(){
        return this.matkul;
    }
    public void cetakMatkul() {
        System.out.println();
        System.out.println("Mata Kuliah: " + this.matkul);
        System.out.println("Dosen Pengajar:" + getDosenPengajar());
    }

}
