package prak5;

public class Mahasiswa {
    private String namaMahasiswa;
    private int nimMahasiswa;
    private MataKuliah[] matkul = new MataKuliah[3];
    public Mahasiswa(){
        namaMahasiswa = "-";
        nimMahasiswa = 0;
    }
    public void setMahasiswa(String nama, int nim, MataKuliah[] matkulIn){
        namaMahasiswa = nama;
        nimMahasiswa = nim;
        matkul = matkulIn;
    }
    public void cetakMhs(){
        System.out.println("Nama Mahasiswa: "+namaMahasiswa);
        System.out.println("NIM Mahasiswa: "+nimMahasiswa);
        System.out.println("Mata kuliah yang dipilih:");
        for(int i = 0; i < 3; i++){
        System.out.println(matkul[i].getMatkul());
        }
    }
}
