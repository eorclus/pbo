package modules;

class Pegawai {
    String nama;
    int umur;
    char jekel;
    public Pegawai(String _nama, int _umur, char _jekel) {
        nama = _nama;
        umur = _umur;
        jekel = _jekel;
    }
    public void printPgw(){
        System.out.println("Nama : "+nama);
        System.out.println("Umur : "+umur);
        System.out.println("Jenis Kelamin : "+jekel);
        System.out.println();
    }
}
