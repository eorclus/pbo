package prak3;

class Calculation2{
    private int a,b;
    private double c,d;

    // method overloading dengan perbedaan tipe data parameter
    void sum(int a, int b){
        System.out.print(a+b);
    }
    void sum(double c, double d){
        System.out.print(c+d);
    }

    public static void main(String[] args){
        Calculation2 obj=new Calculation2();
        obj.sum(10.5,10.5);
        System.out.println();
        obj.sum(20,20);
    }
}
