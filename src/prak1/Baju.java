package prak1;

class BajuClass {
    String warna;
    String kondisi;

    public void newBaju(String warnaf, String kondisif){
        warna=warnaf;
        kondisi=kondisif;
    }

    public void pakaiBaju(){
        kondisi="kotor";
    }

    public void cuciBaju(){
        kondisi="bersih";
    }

    public void tampilBaju(){
        System.out.println("Warna baju : "+warna);
        System.out.println("Kondisi baju : "+kondisi);
        System.out.println();
    }
}

public class Baju {

   public static void main(String[] args){
       BajuClass baju1 = new BajuClass();
       baju1.newBaju("merah","bersih");
       baju1.tampilBaju();
       baju1.pakaiBaju();
       baju1.tampilBaju();
       baju1.cuciBaju();
       baju1.tampilBaju();
   }
}
