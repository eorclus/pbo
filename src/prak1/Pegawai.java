package prak1;
public class Pegawai{
    String NamaPegawai;
    int idPegawai;
    String PosisiPegawai;
    
    Pegawai (String nama, int id, String posisi){
        NamaPegawai = nama;
        idPegawai = id;
        PosisiPegawai = posisi;
    }
    Pegawai(){
        NamaPegawai = "Azkiya";
        idPegawai = 2514;
        PosisiPegawai = "Staff pengajar";
    }
    void Show(){
        System.out.println("Informasi Pegawai");
        System.out.println("Informasi   : "+NamaPegawai);
        System.out.println("Id   : "+idPegawai);
        System.out.println("Posisi   : "+PosisiPegawai);
    }
    public static void main(String args[]){
        Pegawai pegawai1 = new Pegawai();
        Pegawai pegawai2 = new Pegawai("Zahwa",3313,"Staff Akademik");
        pegawai1.Show();
        pegawai2.Show();
    }
}
