package prak1;
public class Siswa{
    String NamaSiswa;
    String AlamatSiswa;
    int NilaiSiswa;
    
    Siswa (String nama, String alamat, int id){
        NamaSiswa = nama;
        AlamatSiswa = alamat;
        NilaiSiswa = id;
    }
    void Show(){
        System.out.println("Informasi Siswa");
        System.out.println("Nama    : "+NamaSiswa);
        System.out.println("Nilai   : "+NilaiSiswa);
        System.out.println("Alamat  : "+AlamatSiswa);
        System.out.println();
    }
    public static void main(String args[]){
        Siswa siswa1 = new Siswa("Zahra","Klaten Tengah",89);
        Siswa siswa2 = new Siswa("Bayu","Klaten Utara",70);
        Siswa siswa3 = new Siswa("Jono","Klaten Utara",76);
        siswa1.Show();
        siswa2.Show();
        siswa3.Show();
    }
}
